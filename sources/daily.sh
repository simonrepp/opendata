#!/usr/bin/env bash

DATA_DIR="$(dirname -- "$( readlink -f -- "$0"; )")/../data/"
DATE=$(date +%Y-%m-%d)

USERS=$(mysql -B --skip-column-names -e "SELECT count(*) FROM gitea.user";)
REPOS=$(mysql -B --skip-column-names -e "SELECT count(*) FROM gitea.repository";)

USAGE_SSD_PERCENT=$(df -h --output=pcent / | tail -n 1)
USAGE_SSD_USED=$(df -h --output=used / | tail -n 1)
USAGE_CEPH_PERCENT=$(df -h --output=pcent /mnt/ceph-cluster | tail -n 1)
USAGE_CEPH_USED=$(df -h --output=used /mnt/ceph-cluster | tail -n 1)

if [[ ! -f "${DATA_DIR}/history.csv" ]]; then
	echo "date, users, repos, usage_ssd_percent, usage_ssd_used, usage_ceph_percent, usage_ceph_used" > "${DATA_DIR}/history.csv"
fi

echo "${DATE}, ${USERS}, ${REPOS}, ${USAGE_SSD_PERCENT}, ${USAGE_SSD_USED}, ${USAGE_CEPH_PERCENT}, ${USAGE_CEPH_USED}" >> "${DATA_DIR}/history.csv"
