const GRAPH_HEIGHT = 280;
const GRAPH_PADDING = 128;

let height = window.innerHeight;
let width = window.innerWidth;

let history;
let maxCount;
let maxDate;
let minCount;
let minDate;

function formatK(count) {
    return `${Math.floor(count / 1000)}k`;
}

async function load() {
    const { HISTORY } = await import(`./history.js`);

    maxCount = 0;
    minCount = Infinity;

    const humanSizeStringToGigabytes = sizeString => {
        if (sizeString.endsWith('G')) {
            return parseFloat(sizeString.replace(/G$/, ''));
        } else if (sizeString.endsWith('T')) {
            return parseFloat(sizeString.replace(/T$/, '')) * 1000;
        } else {
            throw `Format ${sizeString} not yet supported`
        }
    };

    history = HISTORY.map(entry => {
        const mapped = {
            date: new Date(entry.date),
            repos: parseInt(entry.repos),
            usageCephGigabytes: humanSizeStringToGigabytes(entry.usage_ceph_used),
            usageCephPercent: parseInt(entry.usage_ceph_percent.replace(/%$/, '')),
            usageSsdGigabytes: humanSizeStringToGigabytes(entry.usage_ssd_used),
            usageSsdPercent: parseInt(entry.usage_ssd_percent.replace(/%$/, '')),
            users: parseInt(entry.users)
        };

        maxCount = Math.max(maxCount, mapped.repos, mapped.users);
        minCount = Math.min(minCount, mapped.repos, mapped.users);

        return mapped;
    });

    maxDate = history[history.length - 1].date;
    minDate = history[0].date;
}

function render() {
    const timeline = d3.select('#timeline');

    timeline.attr('width', width)
            .attr('height', GRAPH_HEIGHT + 2 * GRAPH_PADDING)

    const wrapper = timeline.select('.wrapper');

    wrapper.attr('transform', `translate(${GRAPH_PADDING},${GRAPH_PADDING})`);

    const dateScale = d3.scaleTime()
        .domain([minDate, maxDate])
        .range([0, width - 2 * GRAPH_PADDING]);

    const percentScale = d3.scaleLinear()
        .domain([0, 100])
        .range([GRAPH_HEIGHT, 0]);

    const countScale = d3.scaleLinear()
        .domain([minCount, maxCount])
        .range([GRAPH_HEIGHT, 0])
        .nice();

    const dateAxis = d3.axisBottom()
        .scale(dateScale);

    const countAxis = d3.axisLeft()
        .tickFormat(formatK)
        .scale(countScale);

    wrapper.select('#axis_date')
        .attr('transform', `translate(0,${GRAPH_HEIGHT})`)
        .call(dateAxis);

    wrapper.select("#axis_count")
        .call(countAxis);

    const countLine = d3.line()
        .curve(d3.curveNatural)
        .x(point => dateScale(point.date))
        .y(point => countScale(point.count));

    wrapper.select('#counts')
        .selectAll('path')
        .data([
            { color: 'turquoise', key: 'repos' },
            { color: 'blue', key: 'users' }
        ], line => line)
        .join('path')
        .attr('fill', 'none')
        .attr('stroke', line => line.color)
        .attr('d', line => {
            const points = history.map(entry => ({
                date: entry.date,
                count: entry[line.key]
            }));

            return countLine(points);
        });

    wrapper.select('#counts')
        .selectAll('text')
        .data([
            { color: 'turquoise', key: 'repos', label: 'Repositories' },
            { color: 'blue', key: 'users', label: 'Users' }
        ], line => line)
        .join('text')
        .attr('class', 'line_label')
        .attr('fill', line => line.color)
        .attr('dominant-baseline', 'middle')
        .attr('x', line => dateScale(history[history.length - 1].date) + 5)
        .attr('y', line => countScale(history[history.length - 1][line.key]))
        .text(line => {
            const lastCount = history[history.length - 1][line.key];
            return `${formatK(lastCount)} ${line.label}`;
        });

    const percentLine = d3.line()
        .curve(d3.curveNatural)
        .x(point => dateScale(point.date))
        .y(point => percentScale(point.percent));

    wrapper.select('#percent')
        .selectAll('path')
        .data([
            { color: 'orange', key: 'usageCephPercent' },
            { color: 'pink', key: 'usageSsdPercent' }
        ], line => line)
        .join('path')
        .attr('fill', 'none')
        .attr('stroke', line => line.color)
        .attr('d', line => {
            const points = history.map(entry => ({
                date: entry.date,
                percent: entry[line.key]
            }));

            return percentLine(points);
        });

    wrapper.select('#percent')
        .selectAll('text')
        .data([
            { color: 'orange', key: 'usageCephPercent', label: 'Usage CEPH' },
            { color: 'pink', key: 'usageSsdPercent', label: 'Usage SSD' }
        ], line => line)
        .join('text')
        .attr('class', 'line_label')
        .attr('fill', line => line.color)
        .attr('dominant-baseline', 'middle')
        .attr('x', line => dateScale(history[history.length - 1].date) + 5)
        .attr('y', line => percentScale(history[history.length - 1][line.key]))
        .text(line => {
            const lastPercent = history[history.length - 1][line.key];
            return `${lastPercent}% ${line.label}`;
        });
}

window.addEventListener('resize', () => {
    width = window.innerWidth;
    height = window.innerHeight;

    render();
});

await load();
render();