# Reads data/history.csv and outputs its contents as data/history.json
# Run this from the root of the repository!
# Usage: python tools/history_to_json.py

import json, re

CSV_FILE = 'data/history.csv'
JSON_FILE = 'data/history.json'

# Turns 'foo, bar\n' into ['foo', 'bar']
def columns(line):
    return re.split('[ \t,]+', line.strip())

with open(CSV_FILE, mode='r', encoding='utf-8') as csv:
    header = columns(csv.readline())
    rows = [columns(line) for line in csv.readlines()]

transformed = [dict(zip(header, columns)) for columns in rows]

with open(JSON_FILE, mode='w', encoding='utf-8') as file:
    file.write(json.dumps(transformed, indent=2))