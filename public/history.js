export const HISTORY = [
  {
    date: "2022-09-07",
    users: "35417",
    repos: "40102",
    usage_ssd_percent: "71%",
    usage_ssd_used: "536G",
    usage_ceph_percent: "6%",
    usage_ceph_used: "376G"
  },
  {
    date: "2022-09-08",
    users: "35496",
    repos: "40204",
    usage_ssd_percent: "72%",
    usage_ssd_used: "537G",
    usage_ceph_percent: "6%",
    usage_ceph_used: "377G"
  },
  {
    date: "2022-09-09",
    users: "35586",
    repos: "40320",
    usage_ssd_percent: "72%",
    usage_ssd_used: "538G",
    usage_ceph_percent: "6%",
    usage_ceph_used: "374G"
  },
  {
    date: "2022-09-10",
    users: "35693",
    repos: "40447",
    usage_ssd_percent: "71%",
    usage_ssd_used: "533G",
    usage_ceph_percent: "6%",
    usage_ceph_used: "375G"
  },
  {
    date: "2022-09-11",
    users: "35785",
    repos: "40543",
    usage_ssd_percent: "71%",
    usage_ssd_used: "533G",
    usage_ceph_percent: "6%",
    usage_ceph_used: "374G"
  },
  {
    date: "2022-09-12",
    users: "35843",
    repos: "40658",
    usage_ssd_percent: "71%",
    usage_ssd_used: "534G",
    usage_ceph_percent: "6%",
    usage_ceph_used: "379G"
  },
  {
    date: "2022-09-13",
    users: "35928",
    repos: "40777",
    usage_ssd_percent: "71%",
    usage_ssd_used: "536G",
    usage_ceph_percent: "6%",
    usage_ceph_used: "378G"
  },
  {
    date: "2022-09-14",
    users: "36037",
    repos: "40890",
    usage_ssd_percent: "72%",
    usage_ssd_used: "540G",
    usage_ceph_percent: "6%",
    usage_ceph_used: "377G"
  },
  {
    date: "2022-09-15",
    users: "36124",
    repos: "40991",
    usage_ssd_percent: "72%",
    usage_ssd_used: "539G",
    usage_ceph_percent: "6%",
    usage_ceph_used: "379G"
  },
  {
    date: "2022-09-16",
    users: "36216",
    repos: "41081",
    usage_ssd_percent: "72%",
    usage_ssd_used: "540G",
    usage_ceph_percent: "6%",
    usage_ceph_used: "380G"
  },
  {
    date: "2022-09-17",
    users: "36295",
    repos: "41154",
    usage_ssd_percent: "72%",
    usage_ssd_used: "542G",
    usage_ceph_percent: "6%",
    usage_ceph_used: "379G"
  },
  {
    date: "2022-09-18",
    users: "36384",
    repos: "41249",
    usage_ssd_percent: "72%",
    usage_ssd_used: "542G",
    usage_ceph_percent: "6%",
    usage_ceph_used: "378G"
  },
  {
    date: "2022-09-19",
    users: "36432",
    repos: "41354",
    usage_ssd_percent: "73%",
    usage_ssd_used: "545G",
    usage_ceph_percent: "6%",
    usage_ceph_used: "377G"
  },
  {
    date: "2022-09-20",
    users: "36498",
    repos: "41458",
    usage_ssd_percent: "73%",
    usage_ssd_used: "545G",
    usage_ceph_percent: "6%",
    usage_ceph_used: "382G"
  },
  {
    date: "2022-09-21",
    users: "36595",
    repos: "41551",
    usage_ssd_percent: "73%",
    usage_ssd_used: "546G",
    usage_ceph_percent: "6%",
    usage_ceph_used: "392G"
  },
  {
    date: "2022-09-22",
    users: "36682",
    repos: "41662",
    usage_ssd_percent: "73%",
    usage_ssd_used: "547G",
    usage_ceph_percent: "6%",
    usage_ceph_used: "391G"
  },
  {
    date: "2022-09-23",
    users: "36777",
    repos: "41761",
    usage_ssd_percent: "73%",
    usage_ssd_used: "549G",
    usage_ceph_percent: "6%",
    usage_ceph_used: "393G"
  },
  {
    date: "2022-09-24",
    users: "36863",
    repos: "41834",
    usage_ssd_percent: "73%",
    usage_ssd_used: "550G",
    usage_ceph_percent: "6%",
    usage_ceph_used: "394G"
  },
  {
    date: "2022-09-25",
    users: "36893",
    repos: "41924",
    usage_ssd_percent: "74%",
    usage_ssd_used: "552G",
    usage_ceph_percent: "6%",
    usage_ceph_used: "400G"
  },
  {
    date: "2022-09-26",
    users: "36959",
    repos: "41990",
    usage_ssd_percent: "75%",
    usage_ssd_used: "560G",
    usage_ceph_percent: "7%",
    usage_ceph_used: "409G"
  },
  {
    date: "2022-09-27",
    users: "37028",
    repos: "42106",
    usage_ssd_percent: "76%",
    usage_ssd_used: "569G",
    usage_ceph_percent: "7%",
    usage_ceph_used: "410G"
  },
  {
    date: "2022-09-28",
    users: "37121",
    repos: "42158",
    usage_ssd_percent: "76%",
    usage_ssd_used: "570G",
    usage_ceph_percent: "7%",
    usage_ceph_used: "410G"
  },
  {
    date: "2022-09-29",
    users: "37201",
    repos: "42237",
    usage_ssd_percent: "77%",
    usage_ssd_used: "574G",
    usage_ceph_percent: "7%",
    usage_ceph_used: "405G"
  },
  {
    date: "2022-09-30",
    users: "37292",
    repos: "42322",
    usage_ssd_percent: "78%",
    usage_ssd_used: "582G",
    usage_ceph_percent: "6%",
    usage_ceph_used: "402G"
  },
  {
    date: "2022-10-01",
    users: "37298",
    repos: "42386",
    usage_ssd_percent: "78%",
    usage_ssd_used: "587G",
    usage_ceph_percent: "6%",
    usage_ceph_used: "395G"
  },
  {
    date: "2022-10-02",
    users: "37360",
    repos: "42454",
    usage_ssd_percent: "78%",
    usage_ssd_used: "589G",
    usage_ceph_percent: "6%",
    usage_ceph_used: "372G"
  },
  {
    date: "2022-10-03",
    users: "37414",
    repos: "42547",
    usage_ssd_percent: "79%",
    usage_ssd_used: "594G",
    usage_ceph_percent: "6%",
    usage_ceph_used: "372G"
  },
  {
    date: "2022-10-04",
    users: "37512",
    repos: "42665",
    usage_ssd_percent: "80%",
    usage_ssd_used: "597G",
    usage_ceph_percent: "6%",
    usage_ceph_used: "366G"
  },
  {
    date: "2022-10-05",
    users: "37582",
    repos: "42739",
    usage_ssd_percent: "79%",
    usage_ssd_used: "591G",
    usage_ceph_percent: "6%",
    usage_ceph_used: "374G"
  },
  {
    date: "2022-10-06",
    users: "37669",
    repos: "42827",
    usage_ssd_percent: "80%",
    usage_ssd_used: "601G",
    usage_ceph_percent: "6%",
    usage_ceph_used: "349G"
  },
  {
    date: "2022-10-07",
    users: "37757",
    repos: "42900",
    usage_ssd_percent: "81%",
    usage_ssd_used: "608G",
    usage_ceph_percent: "6%",
    usage_ceph_used: "342G"
  },
  {
    date: "2022-10-08",
    users: "37848",
    repos: "43010",
    usage_ssd_percent: "83%",
    usage_ssd_used: "624G",
    usage_ceph_percent: "6%",
    usage_ceph_used: "346G"
  },
  {
    date: "2022-10-09",
    users: "37900",
    repos: "43089",
    usage_ssd_percent: "83%",
    usage_ssd_used: "625G",
    usage_ceph_percent: "6%",
    usage_ceph_used: "344G"
  },
  {
    date: "2022-10-10",
    users: "37950",
    repos: "43238",
    usage_ssd_percent: "85%",
    usage_ssd_used: "639G",
    usage_ceph_percent: "6%",
    usage_ceph_used: "340G"
  },
  {
    date: "2022-10-11",
    users: "38031",
    repos: "43371",
    usage_ssd_percent: "86%",
    usage_ssd_used: "649G",
    usage_ceph_percent: "5%",
    usage_ceph_used: "335G"
  },
  {
    date: "2022-10-12",
    users: "38130",
    repos: "43468",
    usage_ssd_percent: "87%",
    usage_ssd_used: "656G",
    usage_ceph_percent: "5%",
    usage_ceph_used: "333G"
  },
  {
    date: "2022-10-13",
    users: "38231",
    repos: "43557",
    usage_ssd_percent: "89%",
    usage_ssd_used: "668G",
    usage_ceph_percent: "5%",
    usage_ceph_used: "308G"
  },
  {
    date: "2022-10-14",
    users: "38330",
    repos: "43652",
    usage_ssd_percent: "89%",
    usage_ssd_used: "670G",
    usage_ceph_percent: "5%",
    usage_ceph_used: "314G"
  },
  {
    date: "2022-10-15",
    users: "38430",
    repos: "43731",
    usage_ssd_percent: "89%",
    usage_ssd_used: "668G",
    usage_ceph_percent: "5%",
    usage_ceph_used: "313G"
  },
  {
    date: "2022-10-16",
    users: "38493",
    repos: "43844",
    usage_ssd_percent: "89%",
    usage_ssd_used: "670G",
    usage_ceph_percent: "5%",
    usage_ceph_used: "315G"
  },
  {
    date: "2022-10-17",
    users: "38545",
    repos: "43933",
    usage_ssd_percent: "90%",
    usage_ssd_used: "673G",
    usage_ceph_percent: "5%",
    usage_ceph_used: "316G"
  },
  {
    date: "2022-10-18",
    users: "38951",
    repos: "44064",
    usage_ssd_percent: "91%",
    usage_ssd_used: "681G",
    usage_ceph_percent: "5%",
    usage_ceph_used: "316G"
  },
  {
    date: "2022-10-19",
    users: "39491",
    repos: "44302",
    usage_ssd_percent: "91%",
    usage_ssd_used: "685G",
    usage_ceph_percent: "5%",
    usage_ceph_used: "330G"
  },
  {
    date: "2022-10-20",
    users: "39626",
    repos: "44456",
    usage_ssd_percent: "92%",
    usage_ssd_used: "688G",
    usage_ceph_percent: "5%",
    usage_ceph_used: "326G"
  },
  {
    date: "2022-10-21",
    users: "39787",
    repos: "44565",
    usage_ssd_percent: "92%",
    usage_ssd_used: "691G",
    usage_ceph_percent: "5%",
    usage_ceph_used: "326G"
  },
  {
    date: "2022-10-22",
    users: "39931",
    repos: "44654",
    usage_ssd_percent: "85%",
    usage_ssd_used: "641G",
    usage_ceph_percent: "7%",
    usage_ceph_used: "408G"
  },
  {
    date: "2022-10-23",
    users: "40016",
    repos: "44767",
    usage_ssd_percent: "78%",
    usage_ssd_used: "583G",
    usage_ceph_percent: "8%",
    usage_ceph_used: "507G"
  },
  {
    date: "2022-10-24",
    users: "40117",
    repos: "44911",
    usage_ssd_percent: "79%",
    usage_ssd_used: "596G",
    usage_ceph_percent: "8%",
    usage_ceph_used: "517G"
  },
  {
    date: "2022-10-25",
    users: "40241",
    repos: "45027",
    usage_ssd_percent: "79%",
    usage_ssd_used: "596G",
    usage_ceph_percent: "8%",
    usage_ceph_used: "517G"
  },
  {
    date: "2022-10-26",
    users: "40361",
    repos: "45163",
    usage_ssd_percent: "81%",
    usage_ssd_used: "604G",
    usage_ceph_percent: "8%",
    usage_ceph_used: "519G"
  },
  {
    date: "2022-10-27",
    users: "40466",
    repos: "45254",
    usage_ssd_percent: "81%",
    usage_ssd_used: "606G",
    usage_ceph_percent: "8%",
    usage_ceph_used: "532G"
  },
  {
    date: "2022-10-28",
    users: "40548",
    repos: "45378",
    usage_ssd_percent: "81%",
    usage_ssd_used: "607G",
    usage_ceph_percent: "8%",
    usage_ceph_used: "534G"
  },
  {
    date: "2022-10-29",
    users: "40659",
    repos: "45560",
    usage_ssd_percent: "75%",
    usage_ssd_used: "563G",
    usage_ceph_percent: "9%",
    usage_ceph_used: "539G"
  },
  {
    date: "2022-10-30",
    users: "40754",
    repos: "45730",
    usage_ssd_percent: "79%",
    usage_ssd_used: "593G",
    usage_ceph_percent: "9%",
    usage_ceph_used: "539G"
  },
  {
    date: "2022-10-31",
    users: "40810",
    repos: "45898",
    usage_ssd_percent: "80%",
    usage_ssd_used: "599G",
    usage_ceph_percent: "9%",
    usage_ceph_used: "539G"
  },
  {
    date: "2022-11-01",
    users: "40910",
    repos: "46018",
    usage_ssd_percent: "80%",
    usage_ssd_used: "602G",
    usage_ceph_percent: "9%",
    usage_ceph_used: "540G"
  },
  {
    date: "2022-11-02",
    users: "40989",
    repos: "46166",
    usage_ssd_percent: "80%",
    usage_ssd_used: "604G",
    usage_ceph_percent: "9%",
    usage_ceph_used: "547G"
  },
  {
    date: "2022-11-03",
    users: "41075",
    repos: "46648",
    usage_ssd_percent: "81%",
    usage_ssd_used: "611G",
    usage_ceph_percent: "9%",
    usage_ceph_used: "552G"
  },
  {
    date: "2022-11-04",
    users: "41177",
    repos: "46848",
    usage_ssd_percent: "82%",
    usage_ssd_used: "616G",
    usage_ceph_percent: "9%",
    usage_ceph_used: "547G"
  },
  {
    date: "2022-11-05",
    users: "41259",
    repos: "46978",
    usage_ssd_percent: "83%",
    usage_ssd_used: "626G",
    usage_ceph_percent: "9%",
    usage_ceph_used: "552G"
  },
  {
    date: "2022-11-06",
    users: "41351",
    repos: "47145",
    usage_ssd_percent: "84%",
    usage_ssd_used: "629G",
    usage_ceph_percent: "9%",
    usage_ceph_used: "553G"
  },
  {
    date: "2022-11-07",
    users: "41432",
    repos: "47267",
    usage_ssd_percent: "85%",
    usage_ssd_used: "635G",
    usage_ceph_percent: "9%",
    usage_ceph_used: "562G"
  },
  {
    date: "2022-11-08",
    users: "41539",
    repos: "47424",
    usage_ssd_percent: "85%",
    usage_ssd_used: "638G",
    usage_ceph_percent: "9%",
    usage_ceph_used: "578G"
  },
  {
    date: "2022-11-09",
    users: "41664",
    repos: "47555",
    usage_ssd_percent: "84%",
    usage_ssd_used: "630G",
    usage_ceph_percent: "9%",
    usage_ceph_used: "584G"
  },
  {
    date: "2022-11-10",
    users: "41787",
    repos: "47756",
    usage_ssd_percent: "85%",
    usage_ssd_used: "638G",
    usage_ceph_percent: "9%",
    usage_ceph_used: "591G"
  },
  {
    date: "2022-11-11",
    users: "41885",
    repos: "47842",
    usage_ssd_percent: "87%",
    usage_ssd_used: "650G",
    usage_ceph_percent: "9%",
    usage_ceph_used: "586G"
  },
  {
    date: "2022-11-12",
    users: "42004",
    repos: "47983",
    usage_ssd_percent: "88%",
    usage_ssd_used: "658G",
    usage_ceph_percent: "10%",
    usage_ceph_used: "658G"
  },
  {
    date: "2022-11-13",
    users: "42127",
    repos: "48101",
    usage_ssd_percent: "90%",
    usage_ssd_used: "675G",
    usage_ceph_percent: "10%",
    usage_ceph_used: "660G"
  },
  {
    date: "2022-11-14",
    users: "42222",
    repos: "48220",
    usage_ssd_percent: "90%",
    usage_ssd_used: "679G",
    usage_ceph_percent: "10%",
    usage_ceph_used: "661G"
  },
  {
    date: "2022-11-15",
    users: "42238",
    repos: "48392",
    usage_ssd_percent: "73%",
    usage_ssd_used: "544G",
    usage_ceph_percent: "12%",
    usage_ceph_used: "783G"
  },
  {
    date: "2022-11-16",
    users: "42432",
    repos: "48552",
    usage_ssd_percent: "17%",
    usage_ssd_used: "127G",
    usage_ceph_percent: "18%",
    usage_ceph_used: "1.2T"
  },
  {
    date: "2022-11-17",
    users: "42662",
    repos: "48734",
    usage_ssd_percent: "5%",
    usage_ssd_used: "36G",
    usage_ceph_percent: "20%",
    usage_ceph_used: "1.3T"
  },
  {
    date: "2022-11-18",
    users: "42662",
    repos: "48734",
    usage_ssd_percent: "5%",
    usage_ssd_used: "33G",
    usage_ceph_percent: "20%",
    usage_ceph_used: "1.3T"
  }
]