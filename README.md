# Codeberg Open Data

This repository provides open data for Codeberg.

The `sources` folder contains scripts for generating the content of the `data` folder on our servers,
the `data` folder thus contains the recorded data,
and the `tools` folder contains script for further procesing (currently only one script for converting the csv-formatted data to json).

This place is, like everything on Codeberg,
absolutely meant for your contribution.
Feel free to add scripts for data generation for the data you are interested in
(feel free to ping us in an issue beforehand,
so we can figure out approaches to obtain this data together).
Also we welcome your addition for data analysis tools,
so everyone can explore what these numbers actually mean.

This repo is maintained by Codeberg,
but developed by the community.

## License

All provided scripts and data are in the public domain (CC0). 

<p xmlns:dct="http://purl.org/dc/terms/" xmlns:vcard="http://www.w3.org/2001/vcard-rdf/3.0#">
  <a rel="license"
     href="http://creativecommons.org/publicdomain/zero/1.0/">
    <img src="https://licensebuttons.net/p/zero/1.0/88x31.png" style="border-style: none;" alt="CC0" />
  </a>
  <br />
  To the extent possible under law,
  <a rel="dct:publisher"
     href="https://codeberg.org/Codeberg-Infrastructure/opendata">
    <span property="dct:title">Codeberg e.V.</span></a>
  has waived all copyright and related or neighboring rights to
  <span property="dct:title">Codeberg Open Data</span>.
This work is published from:
<span property="vcard:Country" datatype="dct:ISO3166"
      content="DE" about="https://codeberg.org/Codeberg-Infrastructure/opendata">
  Germany</span>.
</p>